## 安装说明


```
wget https://openresty.org/download/openresty-1.17.8.2.tar.gz
tar -zxvf ngx_openresty-1.17.8.2.tar.gz
mv openresty-1.17.8.2 openresty
cd openresty
./configure
make && make install
```

## 配置说明


```
user  root;
worker_processes  2;

events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
    sendfile        on;
    keepalive_timeout  65;
    lua_shared_dict my_limit_req_store 100m;
    lua_shared_dict my_limit_conn_store 100m;
    lua_shared_dict my_limit_count_store 100m;
    server {
        listen 80;
        listen 443 ssl;
        server_name  tools.cloudbed.vip;
        ssl on;
        #证书路径
        ssl_certificate    /usr/local/openresty/nginx/cert/1961848_bpmn.52itstyle.vip.pem;
        #私钥路径
        ssl_certificate_key   /usr/local/openresty/nginx/cert/1961848_bpmn.52itstyle.vip.key;
        #缓存有效期
        ssl_session_timeout 5m;
        #可选的加密算法,顺序很重要,越靠前的优先级越高.
        ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
        #安全链接可选的加密协议
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;

        location /{
            proxy_pass http://127.0.0.1:8084; 
        }   
        location /api {
            proxy_pass http://gateway;  
        }  
        # 动静分离 Nginx 处理静态请求，路径自行配置
        location /static {
            root /mnt/domains/tools.cloudbed.vip/;
        }                     
   }
   # 网关集群
   upstream gatway { 
	server 127.0.0.1:8080 max_fails=1 fail_timeout=10s;
   } 
}
```


## 参考


http://openresty.org/en/download.html


