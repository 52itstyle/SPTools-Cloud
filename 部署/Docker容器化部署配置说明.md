## 部署


![输入图片说明](https://images.gitee.com/uploads/images/2020/0622/204343_a8f8f2d8_87650.png "屏幕截图.png")


```
docker run -d -p 8082:8082 \
-v /home/cloud/tools-sys-1.0.0.jar:/usr/tools-sys-1.0.0.jar \
--name tools-sys \
docker.io/openjdk:8 java -jar /usr/tools-sys-1.0.0.jar
```

安装管理面板：


```
docker run -d -p 9000:9000 \
--restart=always \
-v /var/run/docker.sock:/var/run/docker.sock \
--name prtainer \
docker.io/portainer/portainer
```
