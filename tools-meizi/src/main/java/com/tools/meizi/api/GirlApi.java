package com.tools.meizi.api;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.tools.common.core.model.Result;
import com.tools.meizi.service.GirlService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 图床
 */
@Api(tags ="妹子图")
@RestController
@RequestMapping("girl")
public class GirlApi {

    private static final Logger logger = LoggerFactory.getLogger(GirlApi.class);

    @Autowired
    private GirlService meiZiService;

    /**
     * 妹子图
     * @param pageSize
     * @param pageNo
     * @return
     */
    @PostMapping("/{pageSize}/{pageNo}")
    @SentinelResource(value = "girView",fallback = "fallbackHandler", blockHandler = "exceptionHandler")
    public Result list(@PathVariable("pageSize") Integer pageSize,
                       @PathVariable("pageNo") Integer pageNo) {
        return meiZiService.list(pageSize,pageNo);
    }

    /**
     * 限流与阻塞处理
     * @param str
     * @param ex
     */
    public void exceptionHandler(String str, BlockException ex) {
        logger.error( "blockHandler：" + str, ex);
    }

    /**
     * 熔断与降级处理
     * @param str
     */
    public void fallbackHandler(String str) {
        logger.error("fallbackHandler：" + str);
    }

    /**
     * 测试
     * @param id
     * @return
     */
    @GetMapping("/get/{id}")
    public String getMeiZi(@PathVariable("id") Long id){
        return "哈哈哈哈"+id;
    }
}
