package com.tools.meizi.api;

import com.tools.common.core.model.Result;
import com.tools.meizi.service.RecommendService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 今日推荐
 */
@Api(tags ="今日推荐")
@RestController
@RequestMapping("recommend")
public class RecommendApi {

    @Autowired
    private RecommendService recommendService;

    /**
     * 历史干货
     * @param pageSize
     * @param pageNo
     * @return
     */
    @PostMapping("{pageSize}/{pageNo}")
    public Result history(@PathVariable("pageSize") Integer pageSize,
                          @PathVariable("pageNo") Integer pageNo) {
        return recommendService.list(pageSize,pageNo);
    }
    /**
     * 历史干货
     * @return
     */
    @PostMapping("{uuid}")
    public Result history(@PathVariable("uuid") String uuid) {
        return recommendService.get(uuid);
    }
    /**
     * 今日推荐
     * @return
     */
    @PostMapping("today")
    public Result today() {
        return recommendService.today();
    }

    /**
     * 更新阅读数
     * @return
     */
    @PostMapping("view")
    public Result view(String uuid) {
        return recommendService.today();
    }
}
