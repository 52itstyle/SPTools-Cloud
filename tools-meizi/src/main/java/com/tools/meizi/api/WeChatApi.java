package com.tools.meizi.api;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.tools.common.core.model.Result;
import com.tools.meizi.entity.WeChat;
import com.tools.meizi.model.LoginRequest;
import com.tools.meizi.service.PushService;
import com.tools.meizi.service.WeChatService;
import io.swagger.annotations.Api;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 微信小程序
 */
@Api(tags ="微信小程序")
@RestController
@RequestMapping("weChat")
public class WeChatApi {

    @Autowired
    private WxMaService wxService;

    @Autowired
    private WeChatService weChatService;

    @Autowired
    private PushService pushService;

    /**
     * 验证签名
     *
     * @return
     */
    @PostMapping("/msg")
    public String msg(@RequestParam String signature, @RequestParam String timestamp,
                      @RequestParam String nonce, @RequestParam String echostr) {
        Boolean flag = wxService.checkSignature(timestamp, nonce, signature);
        if (flag) {
            return echostr;
        } else {
            return "";
        }
    }

    /**
     * 登录验证
     * @return
     */
    @PostMapping("/login")
    public Result login(LoginRequest request) throws WxErrorException {
        WxMaJscode2SessionResult session =
                wxService.getUserService().getSessionInfo(request.getCode());
        if (null == session) {
            throw new RuntimeException("login handler error");
        }
        return Result.ok(session);
    }
    /**
     * 获取用户
     * @return
     */
    @PostMapping("/getUser")
    public Result getUser(LoginRequest request) throws WxErrorException {
        WxMaJscode2SessionResult session =
                wxService.getUserService().getSessionInfo(request.getCode());
        if (null == session) {
            return Result.error("login handler error");
        }
        // 解密用户信息
        WxMaUserInfo wxUserInfo = wxService.getUserService().getUserInfo(session.getSessionKey(),
                request.getEncryptedData(), request.getIv());
        if (null == wxUserInfo) {
            return Result.error("wxUser not exist");
        }
        return Result.ok(wxUserInfo);
    }

    /**
     * 保存用户
     * @param user
     * @return
     */
    @PostMapping("/save")
    public Result save(WeChat user) {
        return weChatService.save(user);
    }

    /**
     * 订阅消息
     * @param openId
     * @return
     */
    @PostMapping("/subscribe")
    public Result subscribe(String openId) {
        return weChatService.subscribe(openId);
    }

    /**
     * 推送消息
     *
     * @return
     */
    @PostMapping("/push")
    public void push() {
        pushService.push();
    }

}
