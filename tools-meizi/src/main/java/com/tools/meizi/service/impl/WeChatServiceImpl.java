package com.tools.meizi.service.impl;

import com.tools.common.core.constant.SystemConstant;
import com.tools.common.core.model.Result;
import com.tools.common.core.util.DateUtils;
import com.tools.common.dynamicquery.DynamicQuery;
import com.tools.meizi.entity.WeChat;
import com.tools.meizi.service.WeChatService;
import com.vdurmont.emoji.EmojiParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WeChatServiceImpl implements WeChatService {

    @Autowired
    private DynamicQuery dynamicQuery;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result save(WeChat weChat) {
        /**
         * 昵称特殊表情
         */
        weChat.setNickName(EmojiParser.parseToHtmlDecimal(weChat.getNickName()));
        String nativeSql = "SELECT * FROM app_weChat WHERE openid=?";
        WeChat user =
                dynamicQuery.nativeQuerySingleResult(WeChat.class,nativeSql,new Object[]{weChat.getOpenid()});
        if(user!=null){
            weChat.setId(user.getId());
            weChat.setSubscribe(user.getSubscribe());
            dynamicQuery.update(weChat);
        }else{
            weChat.setGmtCreate(DateUtils.getTimestamp());
            weChat.setSubscribe(SystemConstant.SUBSCRIBE_STATUS_NO);
            dynamicQuery.save(weChat);
        }
        return Result.ok("保存成功");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result subscribe(String openId) {
        /**
         * 每订阅一次加一
         */
        String nativeSql = "UPDATE app_weChat SET subscribe=subscribe+1 WHERE openid=?";
        dynamicQuery.nativeExecuteUpdate(nativeSql,new Object[]{openId});
        return Result.ok();
    }
}
