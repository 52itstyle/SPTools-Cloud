package com.tools.meizi.service.impl;

import com.tools.common.core.constant.SystemConstant;
import com.tools.common.core.model.Result;
import com.tools.common.dynamicquery.DynamicQuery;
import com.tools.meizi.entity.Girl;
import com.tools.meizi.service.GirlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GirlServiceImpl implements GirlService {

    @Autowired
    private DynamicQuery dynamicQuery;

    @Override
    public Result list(Integer pageSize, Integer pageNo) {
        String nativeSql = "SELECT * FROM app_girl WHERE status=? GROUP BY id DESC ";
        pageNo = pageNo<=0?0:pageNo-1;
        Pageable pageable = PageRequest.of(pageNo,pageSize);
        List<Girl> list =
                dynamicQuery.nativeQueryPagingList(Girl.class,pageable,nativeSql,new Object[]{SystemConstant.DELETE_STATUS_NO});
        return Result.ok(list);
    }

}
