package com.tools.meizi.service;

import com.tools.common.core.model.Result;

/**
 * 订阅推送
 */
public interface PushService {

    Result push();
}

