package com.tools.meizi.service;

import com.tools.common.core.model.Result;

public interface RecommendService {
    /**
     * 小程序列表
     * @param pageSize
     * @param pageNo
     * @return
     */
    Result list(Integer pageSize, Integer pageNo);

    /**
     * 小程序获取单个
     * @param uuid
     * @return
     */
    Result get(String uuid);

    /**
     * 今日推荐
     * @return
     */
    Result today();
}

