package com.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages="com.tools.*")
@EnableDiscoveryClient
@EnableSwagger2
@EnableFeignClients
public class SysApplication {

    private static final Logger logger = LoggerFactory.getLogger(SysApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SysApplication.class, args);
        logger.info("系统管理启动");
    }
}
