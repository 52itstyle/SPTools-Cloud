package com.tools.sys.controller;

import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysLog;
import com.tools.sys.service.SysLogService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 日志管理
 * 爪哇笔记：https://blog.52itstyle.vip
 */
@Api(tags ="日志管理")
@RestController
@RequestMapping("/log")
public class LogController  {

    @Autowired
    private SysLogService sysLogService;

    /**
     * 日志列表
     */
    @PostMapping("/list")
    public Result list(SysLog log){
        return sysLogService.list(log);
    }

}
