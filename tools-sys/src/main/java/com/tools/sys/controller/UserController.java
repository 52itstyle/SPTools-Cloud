package com.tools.sys.controller;

import com.tools.common.core.config.AbstractController;
import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysUser;
import com.tools.sys.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 用户管理
 * 爪哇笔记：https://blog.52itstyle.vip
 */
@Api(tags ="用户管理")
@RestController
@RequestMapping("/user")
public class UserController extends AbstractController {

    @Autowired
    private SysUserService sysUserService;

    /**
     * 用户列表
     */
    @PostMapping("/list")
    @ApiOperation(value="用户列表")
    public Result list(SysUser user){
        return sysUserService.list(user);
    }

    /**
     * 获取
     */
    @PostMapping("/get")
    @ApiOperation(value="根据用户ID获取用户信息")
    public Result get(Long userId){
        return sysUserService.get(userId);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    @ApiOperation(value="保存用户")
    public Result save(@RequestBody SysUser user){
        return sysUserService.save(user);
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @ApiOperation(value="根据用户ID删除用户信息")
    public Result delete(Long userId){
        return sysUserService.delete(userId);
    }

    /**
     * 修改密码
     */
    @PostMapping("/updatePwd")
    @ApiOperation(value="修改用户密码")
    public Result updatePwd(SysUser user){
        return sysUserService.updatePwd(user);
    }

    /**
     * 根据用户ID获取用户菜单权限
     * @param userId
     * @return
     */
    @ApiOperation(value="根据用户ID获取用户菜单权限")
    @PostMapping("/listUserPerms")
    public List<Object> listUserPerms(Long userId){
        return sysUserService.listUserPerms(userId);
    }

    /**
     * 根据用户ID获取用户角色
     * @param userId
     * @return
     */
    @ApiOperation(value="根据用户ID获取用户角色")
    @PostMapping("/listUserRoles")
    public List<Object> listUserRoles(Long userId){
        return sysUserService.listUserRoles(userId);
    }

}
