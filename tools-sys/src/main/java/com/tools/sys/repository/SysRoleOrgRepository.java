package com.tools.sys.repository;

import com.tools.sys.entity.SysRoleOrg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * sys_role_org Repository
 * Created by 小柒2012
 * Sun Oct 27 13:02:54 CST 2019
*/ 
@Repository 
public interface SysRoleOrgRepository extends JpaRepository<SysRoleOrg, Long> {
}

