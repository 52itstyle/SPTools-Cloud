package com.tools.sys.service;

import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysLog;

public interface SysLogService {

    Result list(SysLog log);
}
