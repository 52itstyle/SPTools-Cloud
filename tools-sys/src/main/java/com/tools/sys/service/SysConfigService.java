package com.tools.sys.service;

import com.tools.common.core.model.Result;
import com.tools.sys.entity.SysConfig;

public interface SysConfigService {

    Result save(SysConfig config);

    Result get(Long id);

    Result delete(Long id);

    Result list(SysConfig config);
}
