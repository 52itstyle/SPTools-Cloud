package com.tools.sys.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "tools-meizi")
@RequestMapping("girl")
public interface GirlFeignService {

    @GetMapping(value = "/get/{id}")
    String getMeiZi(@PathVariable("id") Long id);
}
