package com.tools.meizi.service;

import com.tools.common.core.model.Result;
import com.tools.meizi.entity.Recommend;

public interface RecommendService {
    /**
     * 保存
     * @param recommend
     * @return
     */
    Result save(Recommend recommend);

    /**
     * 修改
     * @param recommend
     * @return
     */
    Result update(Recommend recommend);

    /**
     * 后台列表
     * @param recommend
     * @return
     */
    Result list(Recommend recommend);

    /**
     * 小程序列表
     * @param pageSize
     * @param pageNo
     * @return
     */
    Result list(Integer pageSize, Integer pageNo);


    /**
     * 删除
     * @param id
     * @return
     */
    Result delete(Long id);

    /**
     * 推荐
     * @param id
     * @return
     */
    Result hot(Long id);
    /**
     * 删除全部
     * @param status
     * @return
     */
    Result removeAll(Short status);
}

