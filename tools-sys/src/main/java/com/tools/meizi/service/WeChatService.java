package com.tools.meizi.service;

import com.tools.common.core.model.Result;
import com.tools.meizi.entity.WeChat;

public interface WeChatService {

    Result save(WeChat weChat);

    Result list(WeChat weChat);

    Result delete(Long id);

}

