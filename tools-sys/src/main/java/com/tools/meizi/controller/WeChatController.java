package com.tools.meizi.controller;

import com.tools.common.core.model.Result;
import com.tools.meizi.entity.WeChat;
import com.tools.meizi.service.WeChatService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 用户管理
 */
@Api(tags ="用户管理")
@RestController
@RequestMapping("weChat")
public class WeChatController {

    @Autowired
    private WeChatService weChatService;

    /**
     * 用户列表
     */
    @PostMapping("/list")
    public Result list(WeChat weChat){
        return weChatService.list(weChat);
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public Result delete(Long id){
        return weChatService.delete(id);
    }

}
