package com.tools.meizi.controller;

import com.tools.common.core.model.Result;
import com.tools.meizi.service.PushService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 推送管理
 */
@Api(tags ="推送管理")
@RestController
@RequestMapping("push")
public class PushController {

    @Autowired
    private PushService pushService;
    /**
     * 推送消息
     * @param id
     * @return
     */
    @PostMapping("/today")
    public Result today(Long id) {
        return pushService.today(id);
    }
}
