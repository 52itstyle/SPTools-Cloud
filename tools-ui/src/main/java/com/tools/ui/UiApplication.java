package com.tools.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UiApplication {

    private static final Logger logger = LoggerFactory.getLogger(UiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(UiApplication.class, args);
        logger.info("UI启动");
    }
}
