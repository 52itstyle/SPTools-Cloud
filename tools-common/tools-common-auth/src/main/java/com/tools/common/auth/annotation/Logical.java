package com.tools.common.auth.annotation;

public enum Logical {
    AND,
    OR;

    Logical() {
    }
}
