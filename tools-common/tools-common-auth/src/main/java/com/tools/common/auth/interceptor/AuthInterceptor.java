package com.tools.common.auth.interceptor;

import com.alibaba.fastjson.JSONObject;
import com.tools.common.auth.util.AuthUtils;
import com.tools.common.core.model.Result;
import com.tools.common.core.util.CheckResult;
import com.tools.common.core.util.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 认证拦截
 */
@Component
public class AuthInterceptor implements HandlerInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(AuthInterceptor.class);

    @Autowired
    private AuthUtils authUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler){

        if (handler instanceof HandlerMethod){
            String token = request.getHeader("token");
            if (StringUtils.isEmpty(token)) {
                logger.info("验证失败");
                print(response, Result.error(JwtUtils.JWT_ERROR_CODE_NULL,"签名验证不存在，请重新登录"));
                return false;
            }else{
                CheckResult checkResult = JwtUtils.validateJWT(token);
                if (checkResult.isSuccess()) {
                    /**
                     * 权限验证
                     */
                    String userId = checkResult.getClaims().getId();
                    return authUtils.check(handler,userId);
                } else {
                    switch (checkResult.getErrCode()) {
                        case JwtUtils.JWT_ERROR_CODE_FAIL:
                            logger.info("签名验证不通过");
                            print(response,Result.error(checkResult.getErrCode(),"签名验证不通过，请重新登录"));
                            break;
                        case JwtUtils.JWT_ERROR_CODE_EXPIRE:
                            logger.info("签名过期");
                            print(response,Result.error(checkResult.getErrCode(),"签名过期，请重新登录"));
                            break;
                        default:
                            break;
                    }
                    return false;
                }
            }
        }else{
            return true;
        }
    }
    /**
     * 打印输出
     * @param response
     * @param message  void
     */
    public void print(HttpServletResponse response,Object message){
        try {
            response.setStatus(HttpStatus.OK.value());
            response.setContentType("application/json;charset=UTF-8");
            response.setHeader("Cache-Control", "no-cache, must-revalidate");
            response.setHeader("Access-Control-Allow-Origin", "*");
            PrintWriter writer = response.getWriter();
            writer.write(JSONObject.toJSONString(message));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}